# Projet de RSA : création d'un proxy #

## 1 - Présentation du projet  ##

Un proxy est un serveur particulier dont l'objectif est de relayer les informations entre un client et un autre serveur.  
Le projet réalisé porte sur la création d’un serveur proxy en langage C en utilisant les connaissances acquises durant le module de réseaux et systèmes avancés. En particulier, les sockets ont été utilisées pour le développement du proxy.

## 2 - Utilisation ##

Après avoir fait un Fork et un Clone du projet, il suffit de suivre les étapes suivantes pour faire fonctionner le proxy :  
1. Aller sous un terminal dans le répertoire du projet (le répertoire contenant en particulier le makefile).  
2. Lancer la commande 'make all' afin de compiler le proxy.  
3. Lancer la commande 'make run' afin d'exécuter le proxy.  
4. Sous votre navigateur favori, aller dans les paramètres de gestion de connexion afin de configurer manuellement un proxy. L'adresse du proxy est 127.0.0.1, et le port par défaut utilisé est 2222*.  
5. Si vous chargez une page web, la connexion se fera désormais par le proxy.  
  
*Le port par défaut utilisé est 2222. Il peut être changé : il suffit d'indiquer un autre port dans le makefile (remplacer la première ligne par : 'PORT=NOUVEAU_PORT'), et de spécifier le même port dans le navigateur.  
  
## 3 - Développeurs ##

Julien Feuvrier julien.feuvrier@telecomnancy.eu  
Quentin Seiwert quentin.seiwert@telecomnancy.eu  
  
  
![280px-Telecom_nancy.png](https://bitbucket.org/repo/GBg6eE/images/2996586115-280px-Telecom_nancy.png)
