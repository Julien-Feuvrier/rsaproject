#ifndef PROXY_H_INCLUDED
#define PROXY_H_INCLUDED


/** DEFINES **/
#define DEBUG               0
#define TRUE                1
#define MAXLINE             4096
#define BUFFER_SIZE         1048576
#define MAX_LENGTH_HEADER   10000
#define MAX_LENGTH_URL      2084
#define MAX_LENGTH_HOST     100
#define EXIT_ERROR(msg)     {\
                                perror(msg);\
                                exit(EXIT_FAILURE);\
                            }
#define CONTINUE_ERROR(msg) {\
                                perror(msg);\
                                continue;\
                            }

/** TYPEDEFS **/
typedef struct sockaddr sockaddr;
typedef struct sockaddr_in sockaddr_in;
typedef struct addrinfo addrinfo;


/**
 * \brief generate the url, host and path given the header of a request
 */
void generateUrl(char * header, char * url, char * hostName, char * path);


#endif // PROXY_H_INCLUDED
