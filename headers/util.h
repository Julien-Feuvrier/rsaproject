#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED

/**
 * \brief Ecrire  "n" octets vers un descripteur de socket
 */
int writen (int, char*, int);

/**
 * \brief Lire  "n" octets à partir d'un descripteur de socket
 */
int readn (int, char*, int);

/**
 * \brief Lire  une ligne terminee par \n à partir d'un descripteur de socket
 */
int readline (int, char*, int);

#endif // UTIL_H_INCLUDED
