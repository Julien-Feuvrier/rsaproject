PORT=2222

all: util.o proxy.o
	gcc -Wall obj/*.o -o bin/proxy

util.o:
	gcc -Wall src/util.c -c -o obj/util.o -I headers

proxy.o:
	gcc -Wall src/proxy.c -c -o obj/proxy.o -I headers

run:
	bin/proxy $(PORT)

clean:
	rm -r *~
