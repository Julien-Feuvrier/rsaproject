#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>

#include "util.h"
#include "proxy.h"


int main (int argc, char *argv[])
{
    int proxyServerSocket, clientSocket, serverSocket;
    int retread, childpid, toRead;
    int const yes = 1;
    socklen_t clilen;
    sockaddr_in  serv_addr, cli_addr;
    addrinfo hints, *result, *list_ptr;

    char fromClient[MAXLINE], fromServer[BUFFER_SIZE];
    char header[MAX_LENGTH_HEADER], *ptr;
    char url[MAX_LENGTH_URL], hostName[MAX_LENGTH_HOST], path[MAX_LENGTH_URL];

    // check parameters
    if (argc != 2)
        EXIT_ERROR("Usage : proxy <numPort>\n");

    // socket opening
    if ((proxyServerSocket = socket(PF_INET, SOCK_STREAM, 0)) < 0)
        EXIT_ERROR("servecho : Probleme socket\n");

    // setsockopt to delete the "Address already in use" message
    if (setsockopt(proxyServerSocket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) < 0)
        EXIT_ERROR("setsockopt : erreur set options");

    // bind the socket
    memset((char*)&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl (INADDR_ANY);
    serv_addr.sin_port = htons(atoi(argv[1]));

    if (bind(proxyServerSocket, (sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
        EXIT_ERROR("servecho: erreur bind\n");

    // listen
    if (listen(proxyServerSocket, SOMAXCONN) < 0)
        EXIT_ERROR("servecho: erreur listen\n");

    if (DEBUG)
        printf("En attente de clients...\n");

    // loop which always listen to a client
    while (TRUE)
    {
        *header = '\0';     // reinitialize the header
        clilen = sizeof(cli_addr);

        // accept
        if ((clientSocket = accept(proxyServerSocket, (sockaddr*)&cli_addr, &clilen)) < 0)
            EXIT_ERROR("servecho : erreur accept\n");

        if ((childpid = fork()) < 0)
            EXIT_ERROR("server: fork error\n")
        else if (childpid == 0)
        {
            close (proxyServerSocket);

            // get the request header from the client
            while ((retread = readline(clientSocket, fromClient, MAXLINE)) > 0)
            {
                strcat(header,fromClient);

                if (strcmp (fromClient, "\n") == 3)
                    break ; /* fin de la lecture */
            }

            if (DEBUG)
                printf("=============EN-TETE==============\n%s\n====================================\n",header);

            if (retread < 0 )
                perror ("Erreur readline \n");

            // generate the url, path and hostName
            generateUrl(header, url, hostName, path);

            // initialize hints
            memset(&hints, 0, sizeof(hints));
            hints.ai_family = AF_INET;          // for ipv4 only, AF_UNSPEC for all protocols
            hints.ai_socktype = SOCK_STREAM;
            hints.ai_flags = AI_PASSIVE;
            hints.ai_protocol = 0;
            hints.ai_canonname = NULL;
            hints.ai_addr = NULL;
            hints.ai_next = NULL;

            // get info on the client address, by extracting the Host header
            if (getaddrinfo(hostName, "http", &hints, &result) != 0)
                EXIT_ERROR("Getaddrinfo : Error when get information on address")

            // browse the info list to bind to the server
            for (list_ptr = result;
                 list_ptr != NULL && (serverSocket = socket(list_ptr->ai_family, list_ptr->ai_socktype, list_ptr->ai_protocol)) == -1;
                 list_ptr = list_ptr->ai_next);

            if (list_ptr == NULL)
                EXIT_ERROR("Could not bind to the web server\n");

            // connect to the server (proxy = client for the server)
            if (connect(serverSocket, list_ptr->ai_addr, list_ptr->ai_addrlen) < 0)
            {
                close(serverSocket);
                close(clientSocket);

                EXIT_ERROR("Connect : Error while connecting to the server\n");
            }

            // free the address info
            freeaddrinfo(result);

            // create the request, put in header
            sprintf(header, "GET %s HTTP/1.1\
                            \r\nHost: %s\
                            \r\nProxy-Connection: keep-alive\
                            \r\nConnection: keep-alive\r\n\r\n", path, hostName);

            if (DEBUG)
                printf("Request to the server : \n---------------\n%s\n---------------\n", header);

            // send the request to the server (send header)
            if (writen(serverSocket, header, strlen(header)) != strlen(header))
            {
                close(serverSocket);
                close(clientSocket);

                EXIT_ERROR("Writen : problem while the proxy send data to the server\n");
            }

            // get the response and send it to the client
            toRead = 0;

            if ((retread = read(serverSocket, fromServer, BUFFER_SIZE)) < 0)
                perror ("Erreur readline \n");
            else    // get the quantity of data to read
            {
                fromServer[retread] = '\0';

                if ((ptr = strstr(fromServer, "Content-Length: ")) != NULL)
                {
                    int j, found;

                    toRead = atoi(ptr + 16);

                    // the headers are not in the toRead bytes
                    do
                    {
                        found = 0;

                        for (j = 0 ; ptr[j] != '\n' ; j++)
                            if (ptr[j] == ':')
                                found = 1;

                    } while (found && (ptr += j) < fromServer + retread);

                    toRead -= (retread - (int)(ptr - fromServer));
                }
                else
                {
                    char temp[50] = "", *end;
                    int j = 0, found;

                    for (ptr = fromServer ; ptr < fromServer + retread && *ptr != ':' ; ptr++);

                    while (ptr < fromServer + retread)
                    {
                        found = 0;

                        for (j = 0 ; ptr[j] != '\n' ; j++)
                            if (ptr[j] == ':')
                                found = 1;

                        if (found)  // not the end of the headers, continue the loop
                            ptr = ptr + j + 1;
                        else        // end of the headers, get the hexa size
                        {
                            ptr += 2;

                            for (j = 0 ; (temp[j] = ptr[j]) != '\n' ; j++);

                            temp[j] = '\0';

                            break;
                        }
                    }

                    toRead = (int)strtol(temp, &end, 16) - retread + (int)(end - fromServer);
                }

                writen(clientSocket, fromServer, retread);

                if (DEBUG)
                    printf("%s\n", fromServer);
            }

            while (toRead > 0 && (retread = read(serverSocket, fromServer, BUFFER_SIZE)) > 0) // read the data
            {
                fromServer[retread] = '\0';

                writen(clientSocket, fromServer, retread);

                toRead -= retread;

                if (DEBUG)
                    printf("%s\n", fromServer);
            }

            if (retread < 0)
                perror ("Erreur readline \n");

            if (DEBUG)
                printf("Client Write ended properly\n");

            // close the client and the server
            close(clientSocket);
            close(serverSocket);

            exit (EXIT_SUCCESS);
        }
    }

    // close the proxy
    close(proxyServerSocket);

    return EXIT_SUCCESS;
}

void generateUrl(char * header, char * url, char * hostName, char * path)
{
    char * temp;

    // get the url after a method name
    if ((temp = strstr(header, "GET ")))
        strncpy(url, temp + 4, MAX_LENGTH_URL);
    else if ((temp = strstr(header, "POST ")))
        strncpy(url, temp + 5, MAX_LENGTH_URL);
    else if ((temp = strstr(header, "CONNECT ")))
        strncpy(url, temp + 8, MAX_LENGTH_URL);
    else    // safety
        strncpy(url, (temp = strstr(header, "www.")) ? temp : "", MAX_LENGTH_URL);

    // delete HTTP/1.1 at the end if present, otherwise find \r and \n
    if ((temp = strstr(url, " ")))
        *temp = '\0';
    else if ((temp = strchr(url, '\r')))
        *temp = '\0';
    else if ((temp = strchr(url, '\n')))
        *temp = '\0';
    else    // safety
        *(url + MAX_LENGTH_HOST - 1) = '\0';

    // delete protocol :// if it is present in the url
    if ((temp = strstr(url, "://")))
        strncpy(url, temp + 3, MAX_LENGTH_URL);

    // get the network address
    strncpy(hostName, url, MAX_LENGTH_HOST);

    // delete www. if it is present in the address
    if ((temp = strstr(url, "www.")))
        strncpy(hostName, temp + 4, MAX_LENGTH_HOST);

    // delete the port if it is present in the address
    if ((temp = strchr(hostName, ':')))
        *temp = '\0';

    // delete all the url after the first / to get the final hostName, and in the same time get the path
    if ((temp = strchr(hostName, '/')))
        *temp = '\0';

    // get the path
    if ((temp = strchr(url, '/')))
        strcpy(path, temp);
    else
        strcpy(path, "/");

    if (DEBUG)
        printf("URL : %s\nHostName : %s\nPath : %s\n", url, hostName, path);
}






