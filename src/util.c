/**
 * \brief functions called by the client and the server to emit and receive data
 */

#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#include "util.h"

int writen(int fd, char *ptr, int nbytes)
{
    int nleft = nbytes, nwritten;
    while (nleft > 0)
    {
        nwritten = write (fd,ptr, nleft);
        if (nwritten <= 0)
        {
            if (errno == EINTR)
                nwritten = 0;
            else
            {
                perror("Writen : Problem in write function\n");

                return -1;
            }
        }

        nleft -= nwritten;
        ptr += nwritten;
    }
    return nbytes;
}

int readn(int fd, char *ptr, int maxlen)
{
    int nleft = maxlen, nreadn;

    while (nleft > 0)
    {
        nreadn = read (fd, ptr, nleft);

        if (nreadn < 0)
        {
            if(errno == EINTR)
                nreadn = 0;
            else
            {
                perror("Readn : Problem in read \n");

                return -1;
            }
        }
        else if (nreadn == 0)
            break;

        nleft -= nreadn;
        ptr += nreadn;
    }

    return maxlen - nleft;
}

int readline(int fd, char *ptr, int maxlen)
{
    int n, rc, retvalue, encore = 1;
    char c, *tmpptr = ptr;

    for (n = 1 ; n < maxlen && encore ; n++)
    {
        if ((rc = read (fd, &c, 1)) == 1)
        {
            *tmpptr++ = c;

            if (c == '\n')
            {
                encore = 0;
                retvalue = n;
            }
        }
        else if (rc == 0) // nothing else to read
        {
            encore = 0;

            if (n == 1)  // nothing has been read
                retvalue = 0;
            else
                retvalue = n;
        }
        else if (errno != EINTR)
        {
            encore = 0;
            retvalue = -1;
        }
    }

    *tmpptr = '\0';  // end the line

    return retvalue;
}





